package ex1;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class MyQueueTest {

    @Test
    public void queueUsageExample() {
        MyQueue queue = new MyQueue();

        queue.add(1);
        queue.add(3);
        queue.add(5);

        assertThat(queue.size(), is(3));

        assertThat(queue.takeFirst(), is(1));
        assertThat(queue.takeFirst(), is(3));
        assertThat(queue.takeLast(), is(5));

        assertThat(queue.size(), is(0));
    }

    @Test(expected = IllegalStateException.class)
    public void takingFromEmptyQueueThrows() {
        new MyQueue().takeFirst();
    }
}
